import request from '@/utils/request'

export function getFlowSchemeForEdit(data) {
  return request({
    url: '/api/services/app/FlowScheme/GetFlowSchemeForEdit',
    method: 'get',
    params: data
  })
}
