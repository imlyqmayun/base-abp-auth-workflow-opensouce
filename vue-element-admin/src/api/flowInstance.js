import request from '@/utils/request'

export function createInstance(data) {
  return request({
    url: '/api/services/app/FlowInstance/CreateInstance',
    method: 'post',
    data
  })
}

export function verification(data) {
  return request({
    url: '/api/services/app/FlowInstance/Verification',
    method: 'post',
    data
  })
}

export function getFlowInstanceForEdit(data) {
  return request({
    url: '/api/services/app/FlowInstance/GetFlowInstanceForEdit',
    method: 'get',
    params: data
  })
}

export function queryHistories(data) {
  return request({
    url: '/api/services/app/FlowInstance/QueryHistories',
    method: 'post',
    data
  })
}
