// import rainChartData from './mock/rain-charts'
// import waterChartData from './mock/water-charts'
import { getWaterChartData, getRainChartData } from '@/api/deviceMap'
import waterLine from './echarts-water-line'
import rainBar from './echarts-rain-bar'

var waterRainchart = {
  _fetchWaterData(payload) {
    return new Promise((resolve, reject) => {
      getWaterChartData(payload)
        .then(response => {
          resolve({
            data: [
              ...(response.data &&
                response.data.result &&
                response.data.result.items)
            ]
          })
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  _fetchRainData(payload) {
    return new Promise((resolve, reject) => {
      getRainChartData(payload)
        .then(response => {
          resolve({
            data: [
              ...(response.data &&
                response.data.result &&
                response.data.result.items)
            ]
          })
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // FetchData(stcd, tm) {
  //   return new Promise((resolve, reject) => {
  //     setTimeout(() => {
  //       resolve({
  //         waterData: waterChartData,
  //         rainData: rainChartData
  //       })
  //     }, 300)
  //   })
  // },

  async Render(waterId, rainId, stcd, tm) {
    if (waterId) {
      waterLine.init(waterId)
      waterLine.showLoading()
    }
    if (rainId) {
      rainBar.init(rainId)
      rainBar.showLoading()
    }

    if (waterId) {
      var waterData = await this._fetchWaterData({
        FacNum: stcd,
        StartTM: tm[0],
        EndTM: tm[1]
      })
      waterLine.hideLoading()
      waterLine.update(waterData)
    }
    if (rainId) {
      var rainData = await this._fetchRainData({
        FacNum: stcd,
        StartTM: tm[0],
        EndTM: tm[1]
      })
      rainBar.hideLoading()
      rainBar.update(rainData)
    }
  }
}

export default waterRainchart
