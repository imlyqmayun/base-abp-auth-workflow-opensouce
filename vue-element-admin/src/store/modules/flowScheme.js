﻿import crud from './common/crudCodeBulid'

const flowScheme = {
  namespaced: true,
  state: {
    ...crud.state
  },
  mutations: {
    ...crud.mutations
  },
  actions: {
    ...crud.actions
  }
}

export default flowScheme
