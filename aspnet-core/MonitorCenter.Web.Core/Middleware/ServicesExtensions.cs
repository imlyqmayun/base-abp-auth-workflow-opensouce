﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using MonitorCenter.ETag;

namespace MonitorCenter.Middleware
{
    public static class ServicesExtensions
    {

        public static IServiceCollection AddWebApiEtagCacheHeaders(this IServiceCollection services)
        {
            ServicesExtensions.AddWebApiEtagCacheHeaders(services, null);
            return services;
        }

        public static IServiceCollection AddWebApiEtagCacheHeaders(this IServiceCollection services, Action<ETagEndPointOptions> configureEndPointOptions)
        {
            services.Configure<ETagEndPointOptions>(opt =>
            {
                opt.EndPoints = new DefaultEtagPathList();
            });
            if (configureEndPointOptions != null) services.Configure<ETagEndPointOptions>(configureEndPointOptions);
            return services;
        }
    }
}
