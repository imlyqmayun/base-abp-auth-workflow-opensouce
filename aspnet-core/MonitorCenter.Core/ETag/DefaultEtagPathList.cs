﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonitorCenter.ETag
{
    public class DefaultEtagPathList : List<string>
    {
        public DefaultEtagPathList()
        {
            this.Add("/AbpUserConfiguration/GetAll");
            this.Add("/api/services/app/Session/GetCurrentLoginInformations");
            this.Add("/api/services/app/profile/GetProfilePicture");
        }
    }
}
